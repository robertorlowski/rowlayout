package pl.lt.vaadin.ui.client.rowlayout;

import pl.lt.vaadin.ui.RowLayout;
import pl.lt.vaadin.ui.client.rowlayout.RowLayoutState.CaptionPos;
import pl.lt.vaadin.ui.client.rowlayout.RowLayoutState.ChildComponentData;

import com.google.gwt.dom.client.Element;
import com.vaadin.client.ComponentConnector;
import com.vaadin.client.ConnectorHierarchyChangeEvent;
import com.vaadin.client.ui.button.ButtonConnector;
import com.vaadin.client.ui.checkbox.CheckBoxConnector;
import com.vaadin.client.ui.label.LabelConnector;
import com.vaadin.client.ui.orderedlayout.AbstractOrderedLayoutConnector;
import com.vaadin.client.ui.orderedlayout.Slot;
import com.vaadin.shared.ui.Connect;
import com.vaadin.shared.ui.Connect.LoadStyle;

@SuppressWarnings("serial")
@Connect(value = RowLayout.class, loadStyle = LoadStyle.EAGER)
public class RowLayoutConnector extends AbstractOrderedLayoutConnector {
    
    @Override
    public void onConnectorHierarchyChange(ConnectorHierarchyChangeEvent event) {
        super.onConnectorHierarchyChange(event);

        for (ComponentConnector child : getChildComponents()) {
            updateCaptionPosition(child);
        }
    }
    
    @Override
    public void updateCaption(ComponentConnector connector) {
        super.updateCaption(connector);

        for (ComponentConnector child : getChildComponents()) {
            updateCaptionPosition(child);
        }
    }

    private void updateCaptionPosition(ComponentConnector child) {

        Element element = null;
        String attribute = "";

        ChildComponentData data = getState().data.get(child);
        if (data == null) {
            return;
        }

        Slot slot = getWidget().getSlot(child.getWidget());

        if (child instanceof CheckBoxConnector || child instanceof LabelConnector  || child instanceof ButtonConnector)  {
            element = slot.getElement();
            slot.setCaptionPosition(com.vaadin.client.ui.orderedlayout.CaptionPosition.valueOf(data.captionPosition == null ? CaptionPos.RIGHT.name()
                    : data.captionPosition.name()));

            if (data.captionWidth != null) {
                attribute += "padding-left: " + data.captionWidth + "; ";
            }
            attribute += " text-align: right; vertical-align: middle ";
            
        } else if (slot.hasCaption()) {
            element = slot.getCaptionElement();
            slot.setCaptionPosition(com.vaadin.client.ui.orderedlayout.CaptionPosition.valueOf(data.captionPosition == null ? CaptionPos.LEFT.name()
                    : data.captionPosition.name()));

            if (data.captionWidth != null) {
                attribute += "width: " + data.captionWidth + "; ";
            }
            attribute += " text-align: right; vertical-align: middle ";

        } else {
            return;

        }
        
        if ( element != null && !data.setAttribute && !"".equals(attribute)) {
            element.setAttribute("style", attribute);
        } 
        
        data.setAttribute = true;
    }

    @Override
    public VRowLayout getWidget() {
        return (VRowLayout) super.getWidget();
    }

    @Override
    public RowLayoutState getState() {
        return (RowLayoutState) super.getState();
    }

}
