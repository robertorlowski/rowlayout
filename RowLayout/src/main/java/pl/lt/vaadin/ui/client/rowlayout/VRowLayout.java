package pl.lt.vaadin.ui.client.rowlayout;

import com.vaadin.client.StyleConstants;
import com.vaadin.client.ui.orderedlayout.VAbstractOrderedLayout;

public class VRowLayout extends VAbstractOrderedLayout {

    public static final String CLASSNAME = "v-rowlayout";

    /**
     * Default constructor
     */
    public VRowLayout() {
        super(false);
        setStyleName(CLASSNAME);
    }

    @Override
    public void setStyleName(String style) {
        super.setStyleName(style);
        addStyleName(StyleConstants.UI_LAYOUT);
        addStyleName("v-horizontal");
    }

}
