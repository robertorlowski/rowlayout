package pl.lt.vaadin.ui;

import pl.lt.vaadin.ui.client.rowlayout.RowLayoutState;
import pl.lt.vaadin.ui.client.rowlayout.RowLayoutState.CaptionPos;
import pl.lt.vaadin.ui.client.rowlayout.RowLayoutState.ChildComponentData;

import com.vaadin.ui.AbstractOrderedLayout;
import com.vaadin.ui.Component;

@SuppressWarnings("serial")
public class RowLayout extends AbstractOrderedLayout {

    private String defaultCaptionWidth = RowLayoutState.DEFAULT_COLUMN_WIDTH;
    private CaptionPos defaultCaptionPosition = null;

    public RowLayout(String captionWidth) {
        super();
        this.defaultCaptionWidth = captionWidth;
        
        setWidth(100, Unit.PERCENTAGE);
        setSpacing(true);
    }

    public RowLayout() {
        this(RowLayoutState.DEFAULT_COLUMN_WIDTH);
    }

    public void addComponent(Component c) {
        this.addComponent(c, defaultCaptionWidth);
    }

    public void addComponentAsFirst(Component c, String captionWidth) {
        super.addComponentAsFirst(c);
        registerCaptionWidth(c, captionWidth, defaultCaptionPosition);
    }

    public void addComponent(Component c, String captionWidth) {
        super.addComponent(c);
        registerCaptionWidth(c, captionWidth, defaultCaptionPosition);
    }

    public void addComponent(Component c, String captionWidth, CaptionPos captionPosition) {
        super.addComponent(c);
        registerCaptionWidth(c, captionWidth, captionPosition);
    }

    public void removeComponent(Component c) {
        super.removeComponent(c);
        getState().data.remove(c);
    }

    public void replaceComponent(Component oldComponent, Component newComponent, String captionWidth) {
        super.replaceComponent(oldComponent, newComponent);
        getState().data.remove(oldComponent);
        registerCaptionWidth(newComponent, captionWidth, defaultCaptionPosition);
    }

    private void registerCaptionWidth(Component c, String captionWidth, CaptionPos captionPosition) {
        ChildComponentData data = new ChildComponentData();
        data.captionWidth = captionWidth;
        data.captionPosition = captionPosition;
        getState().data.put(c, data);
    }

    public void setDefaultCaptionWidth(String defaultCaptionWidth) {
        this.defaultCaptionWidth = defaultCaptionWidth;
    }

    public void setDefaultCaptionPosition(CaptionPos defaultCaptionPosition) {
        this.defaultCaptionPosition = defaultCaptionPosition;
    }

    @Override
    protected RowLayoutState getState() {
        return (RowLayoutState) super.getState();
    }

    @Override
    protected RowLayoutState getState(boolean markAsDirty) {
        return (RowLayoutState) super.getState(markAsDirty);
    }
}
