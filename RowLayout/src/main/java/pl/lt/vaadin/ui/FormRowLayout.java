package pl.lt.vaadin.ui;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import pl.lt.vaadin.ui.client.rowlayout.RowLayoutState;
import pl.lt.vaadin.ui.client.rowlayout.RowLayoutState.CaptionPos;

import com.vaadin.ui.Component;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class FormRowLayout extends VerticalLayout {

    private static final int DEFAULT_COLUMN_COUNT = 1;
    
    private List<RowLayout> layouts = new LinkedList<RowLayout>();
    
    private int column = DEFAULT_COLUMN_COUNT;
    private String defaultCaptionWidth = RowLayoutState.DEFAULT_COLUMN_WIDTH;
    
    /**
     * Constructs a new component FormRowLayout container.
     */
    public FormRowLayout() {
        this(DEFAULT_COLUMN_COUNT, "100%");
    }

    /**
     * Constructs a new component FormRowLayout container.
     */
    public FormRowLayout(int column, String defaultCaptionWidth) {
        super();
        
        this.column = column;
        this.defaultCaptionWidth = defaultCaptionWidth;

        setWidth(100, Unit.PERCENTAGE);
        setSpacing(true);
    }

    /**
     * Constructs a new component FormRowLayout container.
     */
    public FormRowLayout(Component... children) {
        this();
        
        for (Component c : children  ) {
            this.addComponents(c);    
        }
    }
    
    @Override
    public void addComponent(Component component) {
        addComponent(component, defaultCaptionWidth);
    }

    public RowLayout addComponent(Component component, String captionWidth) {
        return addComponent(component, captionWidth, null);
    }
    
    public RowLayout addComponent(Component component, String captionWidth, CaptionPos captionPosition) {
        RowLayout rowLayout = null;

        if ( component == null || captionWidth == null) {
            throw new IllegalArgumentException();
        }
        
        if (layouts.isEmpty()) {
            rowLayout = new RowLayout(captionWidth);

            layouts.add(rowLayout);
            super.addComponent(rowLayout);

        } else if ( layouts.get(layouts.size() - 1).getComponentCount() != 0 && 
                    layouts.get(layouts.size() - 1).getComponentCount() % column == 0) {
            
            rowLayout = new RowLayout(captionWidth);

            layouts.add(rowLayout);
            super.addComponent(rowLayout);

        } else {
            rowLayout = layouts.get(layouts.size() - 1);
        }
        
        rowLayout.addComponent(component, captionWidth, captionPosition);        
        return rowLayout;
    }

    @Override
    public void removeComponent(Component component) {
        for (final Iterator<RowLayout> row = layouts.iterator(); row.hasNext();) {
            RowLayout rowLayout = row.next();

            if ( rowLayout.equals(component) ) {
                rowLayout.removeAllComponents();
            
            } else {
                
                for (final Iterator<Component> i = rowLayout.iterator(); i.hasNext();) {
                    Component ccc = i.next();
                    if (component.equals(ccc)) {
                        rowLayout.removeComponent(component);
                        break;
                    }
                }                
            }
            
            if (rowLayout.getComponentCount() == 0) {
                super.removeComponent(rowLayout);
                layouts.remove(rowLayout);
                break;
            }           
        }
        
        super.removeComponent(component);
    }

    /**
     * Removes all components from the container. This should probably be
     * re-implemented in extending classes for a more powerful implementation.
     */
    @Override
    public void removeAllComponents() {
        final LinkedList<Component> l = new LinkedList<Component>();

        // Adds all components
        for (final Iterator<Component> i = iterator(); i.hasNext();) {
            l.add(i.next());
        }

        // Removes all component
        for (final Iterator<Component> i = l.iterator(); i.hasNext();) {
            removeComponent(i.next());
        }
        
        layouts.clear();       
        super.removeAllComponents();
    }
    
    public RowLayout addRow(int column) {
        return addRow(column, this.defaultCaptionWidth);
    }
    
    public RowLayout addRow(int column, String defaultCaptionWidth) {
        this.defaultCaptionWidth = defaultCaptionWidth;
        this.column = column;
        
        RowLayout rowLayout = new RowLayout(this.defaultCaptionWidth);

        layouts.add(rowLayout);
        super.addComponent(rowLayout);
        
        return rowLayout;
    }
    
    public RowLayout getRowLayout( Component component ) {
        for (final Iterator<RowLayout> row = layouts.iterator(); row.hasNext();) {
            RowLayout rowLayout = row.next();

            if ( rowLayout.equals(component) ) {
                return rowLayout;            
            } 
            
            for (final Iterator<Component> i = rowLayout.iterator(); i.hasNext();) {
                Component ccc = i.next();
                if (component.equals(ccc)) {
                    return rowLayout;
                }
            }
        }
        return null;
    }
    
    /**
     * Set the component column
     * 
     * @param column
     */
    public void setColumn(int column) {
        this.column = column;
    }
    
    public void setDefaultCaptionWidth(String defaultCaptionWidth) {
        this.defaultCaptionWidth = defaultCaptionWidth;
    }
    
    public List<RowLayout> getRowLayouts() {
        return layouts;
    }

    
}
