package pl.lt.vaadin.ui.client.rowlayout;

import java.io.Serializable;
import java.util.HashMap;

import com.vaadin.shared.Connector;
import com.vaadin.shared.ui.orderedlayout.AbstractOrderedLayoutState;

@SuppressWarnings("serial")
public class RowLayoutState extends AbstractOrderedLayoutState {
    {
        primaryStyleName = "v-rowlayout";
    }

    public enum CaptionPos {
        TOP, RIGHT, BOTTOM, LEFT
    }
    
    public static final String DEFAULT_COLUMN_WIDTH = "100px";
    
    public HashMap<Connector, ChildComponentData> data = new HashMap<Connector, ChildComponentData>();

    public static class ChildComponentData implements Serializable {
        
        public boolean setAttribute = false;
        public String captionWidth = DEFAULT_COLUMN_WIDTH;
        public CaptionPos captionPosition = null;

    }

}
