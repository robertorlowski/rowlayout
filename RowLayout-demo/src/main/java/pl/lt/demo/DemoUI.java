package pl.lt.demo;

import javax.servlet.annotation.WebServlet;

import pl.lt.vaadin.ui.FormRowLayout;
import pl.lt.vaadin.ui.RowLayout;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Theme("reindeer")
@Title("RowLayout Add-on Demo")
@SuppressWarnings("serial")
public class DemoUI extends UI {

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = DemoUI.class, widgetset = "pl.lt.demo.DemoWidgetSet")
	public static class Servlet extends VaadinServlet {
	}

	@Override
	protected void init(VaadinRequest request) {

		final VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.setSizeUndefined();
		mainLayout.setSpacing(true);
		mainLayout.setMargin(true);

		mainLayout.addComponent(new Label( "<b>Example: RowLayout</b>", ContentMode.HTML));
		
		RowLayout rowLayout = new RowLayout();
		rowLayout.addComponent(new TextField("TextField 1 long caption"), "150px");
		rowLayout.addComponent(new TextField("TextField 2"), "100px");
		rowLayout.addComponent(new TextField("TextField 3"), "100px");
		mainLayout.addComponent(rowLayout);
		
		mainLayout.addComponent(new Label( "<p/><b>Example: FormRowLayout</b>", ContentMode.HTML));
		final FormRowLayout formRowLayout = new FormRowLayout(2, "80px");	

		formRowLayout.addComponent(new TextField("Component 1"));
		formRowLayout.addComponent(new TextField("Component 2"));
		ComboBox cmbBox = new ComboBox("Component 3");
		cmbBox.setWidth("140px");
		formRowLayout.addComponent(cmbBox, "80px");
		formRowLayout.addComponent(new CheckBox("Component 4"), "85px");
		formRowLayout.addComponent(new Label("Label 1"), "10px");
		formRowLayout.addComponent(new Label("Label 2"), "265px");
		
		mainLayout.addComponent(formRowLayout);

		setContent(mainLayout);
	}
}
